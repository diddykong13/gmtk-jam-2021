﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    private BoxCollider box;

    public float hitboxDelayInSeconds = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        box = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision hit)
    {

    }

    public void Fire()
    {
        if (box.enabled)
        {
            return;
        }
        box.enabled = true;
        StartCoroutine(DisableHitBoxAfterTime(hitboxDelayInSeconds));
    }

    IEnumerator DisableHitBoxAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        box.enabled = false;
    }
}
