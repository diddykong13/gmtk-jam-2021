﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourcesHolder
{
    public static int WOOD = 0;
    public static int STONE = 0;
    public static int ORE = 0;
    public static int ICE = 0;
    public static int VINE = 0;

    public static int WOOD_REQUIRED = 10;
    public static int STONE_REQUIRED = 6;
    public static int ORE_REQUIRED = 3;
    public static int ICE_REQUIRED = 15;
    public static int VINE_REQUIRED = 12;


    public static void DebugAmounts()
    {
        Debug.Log("Wood: " + WOOD);
        Debug.Log("Stone: " + STONE);
        Debug.Log("Ore: " + ORE);
        Debug.Log("Ice: " + ICE);
        Debug.Log("Vine: " + VINE);
    }
}
