﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpToPosition : MonoBehaviour
{
    public float timeOfTravel = 1; //time after object reach a target place 
    private float currentTime = 0; // actual floting time 
    private float normalizedValue;
    private RectTransform rectTransform; //getting reference to this component 

    public Vector3 startPosition;
    public Vector3 endPosition;

    private bool triggered = false;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        
    }
    IEnumerator LerpObject()
    {
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, currentTime));//  currentTime / timeOfTravel; // we normalize our time 

            rectTransform.anchoredPosition = Vector3.Lerp(startPosition, endPosition, normalizedValue);
            yield return null;
        }
    }

    void Update()
    {
        if (Input.anyKey && !triggered)
        {
            triggered = true;
            StartCoroutine(LerpObject());
        }
    }
}