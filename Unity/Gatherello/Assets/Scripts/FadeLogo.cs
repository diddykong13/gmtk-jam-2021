﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class FadeLogo : MonoBehaviour
{
    public float targetValue = 0;
    public float fadeSpeed = 1;
    RawImage elementToFade;

    protected bool fadeStarted = false;

    void Start()
    {
        
    }

    protected void BeginFade()
    {
        elementToFade = gameObject.GetComponent<RawImage>();
        StartCoroutine(LerpFunction(targetValue, fadeSpeed));
    }

    void Update()
    {
        if (Input.anyKey && !fadeStarted)
        {
            fadeStarted = true;
            BeginFade();
        }
    }

    IEnumerator LerpFunction(float endValue, float duration)
    {
        float time = 0;
        float startValue = elementToFade.color.a;

        while (time < duration)
        {
            elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, (Mathf.Lerp(startValue, endValue, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, time / duration)))));

            time += Time.deltaTime;
            yield return null;
        }
        elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, endValue);
    }
}