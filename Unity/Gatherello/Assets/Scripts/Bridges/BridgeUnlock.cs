﻿using UnityEngine;

public class BridgeUnlock : MonoBehaviour
{

    public Material revealedMaterial;
    public MeshRenderer meshRenderer;
    public GameObject highlight;
    public GameObject[] bounds;

    public int resourceType = 0;

    // Local debug to be removed
    public bool debugUnlock = false;
    private bool debugUnlocked = false;

    private bool highlightEnabled = false;

    private bool unlocked = false;

    private Animator animator;
    public ParticleSystem pfx;
    private AudioSource audioData;


    // Start is called before the first frame update
    void Start()
    {
        if(highlight){
            highlight.SetActive(false);
        }

        animator = GetComponent<Animator>();
        audioData = GetComponent<AudioSource>();
    }


    protected bool HasResourceAmount()
    {
        switch (resourceType)
        {
            case 0:
                if(ResourcesHolder.WOOD >= ResourcesHolder.WOOD_REQUIRED)
                {
                    ResourcesHolder.WOOD -= ResourcesHolder.WOOD_REQUIRED;
                    return true;
                }
                return false;
            case 1:
                if (ResourcesHolder.STONE >= ResourcesHolder.STONE_REQUIRED)
                {
                    ResourcesHolder.STONE -= ResourcesHolder.STONE_REQUIRED;
                    return true;
                }
                return false;
            case 2:
                if (ResourcesHolder.ORE >= ResourcesHolder.ORE_REQUIRED)
                {
                    ResourcesHolder.ORE -= ResourcesHolder.ORE_REQUIRED;
                    return true;
                }
                return false;
            case 3:
                if (ResourcesHolder.ICE >= ResourcesHolder.ICE_REQUIRED)
                {
                    ResourcesHolder.ICE -= ResourcesHolder.ICE_REQUIRED;
                    return true;
                }
                return false;
            case 4:
                if (ResourcesHolder.VINE >= ResourcesHolder.VINE_REQUIRED)
                {
                    ResourcesHolder.VINE -= ResourcesHolder.VINE_REQUIRED;
                    return true;
                }
                return false;
        }
        return false;
    }


    private void OnTriggerEnter(Collider other)
    {
        var hitByPlayer = other.transform.gameObject.tag.Equals("InteractionCollider");

        if (hitByPlayer && !unlocked)
        {
            if (HasResourceAmount())
            {
                Unlock();
            }
        }
    }


    protected void Unlock()
    {
        unlocked = true;
        meshRenderer.material = revealedMaterial;
        highlight.SetActive(false);
        audioData.Play(0);
        animator.SetTrigger("Build");
    }
    public void OnBuilding()
    {
        if (pfx)
        {
            pfx.Play();
        }
    }

    public void OnBuildFinished()
    {
        foreach (GameObject bound in bounds)
        {
            bound.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(debugUnlock && !debugUnlocked)
        {
            debugUnlocked = true;
            Unlock();
        }

        if (CheckResourceAmount() && !highlightEnabled)
        {
            highlightEnabled = true;
            highlight.SetActive(true);
        }
    }


    protected bool CheckResourceAmount()
    {
        switch (resourceType)
        {
            case 0:
                return ResourcesHolder.WOOD >= ResourcesHolder.WOOD_REQUIRED;
            case 1:
                return ResourcesHolder.STONE >= ResourcesHolder.STONE_REQUIRED;
            case 2:
                return ResourcesHolder.ORE >= ResourcesHolder.ORE_REQUIRED;
            case 3:
                return ResourcesHolder.ICE >= ResourcesHolder.ICE_REQUIRED;
            case 4:
                return ResourcesHolder.VINE >= ResourcesHolder.VINE_REQUIRED;
        }
        return false;
    }


}
