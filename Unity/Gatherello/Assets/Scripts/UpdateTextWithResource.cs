﻿using UnityEngine;
using UnityEngine.UI;

public class UpdateTextWithResource : MonoBehaviour
{

    public Text text_current;
    public Text text_required;
    public int resourceType = 0;

    // Update is called once per frame
    void Update()
    {
        switch(resourceType)
        {
            case 0:
                text_current.text = ResourcesHolder.WOOD.ToString();
                text_required.text = ResourcesHolder.WOOD_REQUIRED.ToString();
                break;
            case 1:
                text_current.text = ResourcesHolder.STONE.ToString();
                text_required.text = ResourcesHolder.STONE_REQUIRED.ToString();
                break;
            case 2:
                text_current.text = ResourcesHolder.ORE.ToString();
                text_required.text = ResourcesHolder.ORE_REQUIRED.ToString();
                break;
            case 3:
                text_current.text = ResourcesHolder.ICE.ToString();
                text_required.text = ResourcesHolder.ICE_REQUIRED.ToString();
                break;
            case 4:
                text_current.text = ResourcesHolder.VINE.ToString();
                text_required.text = ResourcesHolder.VINE_REQUIRED.ToString();
                break;
        }
    }
}
