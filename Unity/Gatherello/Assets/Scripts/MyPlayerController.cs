﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 15f;
    public float gravityScale = 5f;
    public float turnCoefficient = 0.1f;

    private Transform mainCamera;
    private float turnSmoothVelocity;
    private CharacterController controller;
    private Vector3 moveDirection;

    void Start()
    {
        mainCamera = Camera.main.transform;
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(0, moveDirection.y, 0);

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + mainCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnCoefficient);
            transform.rotation = Quaternion.Euler(0.0f, angle, 0.0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            moveDir = moveDir.normalized * moveSpeed;

            moveDirection.x = moveDir.x;
            moveDirection.z = moveDir.z;
        }


        if (controller.isGrounded)
        {
            moveDirection.y = 0;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpForce;
            }
        }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * Time.deltaTime * gravityScale);

        controller.Move(moveDirection * Time.deltaTime);
    }
}