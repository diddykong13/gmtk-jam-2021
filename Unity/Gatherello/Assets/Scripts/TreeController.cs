﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
    WOOD = 0,
    STONE = 1,
    ORE = 2,
    ICE = 3,
    VINE = 4
}

public class TreeController : MonoBehaviour
{
    Animator animator;

    public ParticleSystem highlightPx;
    public ParticleSystem harvestPx;

    public int resourceAmount = 3;

    public ResourceType resourceType = ResourceType.WOOD;
    public int harvestValue = 1;

    public float harvestDelayInSeconds = 1;

    private bool canBeHit = true;

    private AudioSource audioData;

    private void OnTriggerEnter(Collider other)
    {
        var hitByPlayer = other.transform.gameObject.tag.Equals("InteractionCollider");

        if (hitByPlayer && canBeHit)
        {
            if (resourceAmount > 0)
            {
                resourceAmount -= harvestValue;
                canBeHit = false;
                animator.SetTrigger("Harvest");
                // play sound
                audioData.Play(0);
                StartCoroutine(MakeHarvestableAfterTime(harvestDelayInSeconds));
            }
        }
    }

    IEnumerator MakeHarvestableAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        canBeHit = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    public void OnHarvest()
    {
        harvestPx.Play();
        AddResource(resourceType, harvestValue);
    }

    public void OnDepleted()
    {
        if (resourceAmount <= 0)
        {
            animator.SetTrigger("Disappear");
        }
    }

    public void OnDisappear()
    {
        highlightPx.Stop();
    }

    private void AddResource(ResourceType type, int value)
    {
        switch (type)
        {
            case ResourceType.WOOD:
                {
                    ResourcesHolder.WOOD += value;
                }
                break;
            case ResourceType.ORE:
                {
                    ResourcesHolder.ORE += value;
                }
                break;
            case ResourceType.ICE:
                {
                    ResourcesHolder.ICE += value;
                }
                break;
            case ResourceType.STONE:
                {
                    ResourcesHolder.STONE += value;
                }
                break;
            case ResourceType.VINE:
                {
                    ResourcesHolder.VINE += value;
                }
                break;
        }

        ResourcesHolder.DebugAmounts();
    }
}
