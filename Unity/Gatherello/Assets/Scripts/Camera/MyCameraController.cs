﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCameraController : MonoBehaviour
{
    public Transform target;

    protected Vector3 offset;
    public float elevation = -2f;
    public float distance = 2.5f;

    public Transform light1;
    public Transform light2;

    protected int rotationState = 0; //0,1,2,3

    void Start()
    {
        offset = new Vector3(distance, elevation, -distance);
    }

    void Rotate(int direction)
    {
        rotationState = (rotationState + direction) % 4;
        if(rotationState < 0) { rotationState = 3; }
        switch(rotationState)
        {
            case 0:
                offset = new Vector3(distance, elevation, -distance);
                light1.rotation = Quaternion.Euler(new Vector3(light1.rotation.eulerAngles.x, 130, light1.rotation.eulerAngles.z));
                //130
                break;
            case 1:
                offset = new Vector3(-distance, elevation, -distance);
                light1.rotation = Quaternion.Euler(new Vector3(light1.rotation.eulerAngles.x, 220, light1.rotation.eulerAngles.z));
                //220
                break;
            case 2:
                offset = new Vector3(-distance, elevation, distance);
                light1.rotation = Quaternion.Euler(new Vector3(light1.rotation.eulerAngles.x, 310, light1.rotation.eulerAngles.z));
                //310
                break;
            case 3:
                offset = new Vector3(distance, elevation, distance);
                light1.rotation = Quaternion.Euler(new Vector3(light1.rotation.eulerAngles.x, 400, light1.rotation.eulerAngles.z));
                //400
                break;
        }
    }

    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Q))
        {
            Rotate(1);
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            Rotate(-1);
        }

        transform.position = target.position - offset;

        transform.LookAt(target);
    }
}