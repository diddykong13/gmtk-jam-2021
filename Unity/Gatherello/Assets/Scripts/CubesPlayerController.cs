using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesPlayerController : MonoBehaviour
{
    public Transform mainCamera;
    public float moveSpeed = 5f;
    public float jumpForce = 15f;
    public float gravityScale = 5f;
    public float turnCoefficient = 0.1f;
    public float landingDelay = 0.2f;
    
    private float turnSmoothVelocity;
    private CharacterController controller;
    private Vector3 moveDirection;
    private Animator animator;

    public InteractionController interactionController;

    bool isInAir = false;
    bool canMove = true;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(0, moveDirection.y, 0);

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (controller.isGrounded && isInAir)
        {
            animator.SetTrigger("Land");
            isInAir = false;
            canMove = false;
            StartCoroutine(ResumeMovementAfterTime(landingDelay));
        }

        if (direction.magnitude >= 0.1f && canMove && !isInAir)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + mainCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnCoefficient);
            transform.rotation = Quaternion.Euler(0.0f, angle, 0.0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            moveDir = moveDir.normalized * moveSpeed;

            moveDirection.x = moveDir.x;
            moveDirection.z = moveDir.z;
            animator.SetBool("IsMoving", true);

        } else 
        {
            animator.SetBool("IsMoving", false);
        }

        if (controller.isGrounded && canMove)
        {
            moveDirection.y = 0;
            if (Input.GetButton("Jump"))
            {
                isInAir = true;
                animator.SetTrigger("Jump");
                moveDirection.y = jumpForce;
            } else if (Input.GetButton("Fire1"))
            {                
                interactionController.Fire();
            }
        }

        if(isInAir)
        {
            moveDirection += transform.forward * moveSpeed;
        }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * Time.deltaTime * gravityScale);

        controller.Move(moveDirection * Time.deltaTime);
    }

    IEnumerator ResumeMovementAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        canMove = true;
    }

}
